import * as data from "./data/persons.json"
import Rx from "rx";

class StateManagerClass {
    static _state = {}
    _state$ = null;

    constructor(){
        this._state = {
            persons: data.default
        }
        this._state$ = new Rx.BehaviorSubject(this._state);
        
    }

    updatePersons(index, person){
        this._state.persons[index] = person;
        this._state$.onNext(this._state)
    }

    deletePerson(index){
        this._state.persons.splice(index,1)
        this._state$.onNext(this._state)
    }

    addPerson(person){
        this._state.persons.push(person)
        this._state$.onNext(this._state)
    }

    getObservableState(){
        return this._state$;
    }
}

export const StateManager = new StateManagerClass();

