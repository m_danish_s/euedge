import React, { Component } from 'react';
import TableComponent from "./components/table_component/TableComponent";
import DataDumpComponent from "./components/data_dump_component/DataDumpComponent";
import FormComponent from "./components/form_component/FormComponent";
import GraphComponent from "./components/graph_component/GraphComponent";
import {StateManager} from "./AppState.js"


class App extends Component {

  constructor(props) {
    super(props)
    this.state={
      persons: []
    }
  }

  componentDidMount() {
    StateManager.getObservableState().subscribe(state=>{
      this.setState({
        persons:state.persons
      })
    })
  }

  render() {
    return (
      <div className="container app">
        <div className="row">
          <div className="col-md-12">
            <button type="button" className="btn btn-primary action-btn" data-toggle="modal" data-target="#add_person_modal">Add</button>
            <button type="button" className="btn btn-primary" data-toggle="modal" data-target="#graph_modal">Show Graph</button>
            <FormComponent />
            <GraphComponent data={this.state.persons} />
          </div>
          <div className="col-md-12  person-table">
            <TableComponent data={this.state.persons} />
          </div>
          <div className="col-md-12">
            <strong>Data Dump</strong>
            <DataDumpComponent data={this.state.persons} />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
