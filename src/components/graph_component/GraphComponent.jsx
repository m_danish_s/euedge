import React from "react"
import { Chart } from 'react-google-charts';

export default class FormComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            graphData: this.generateGraphData(props.data)
        }
    }

    componentWillReceiveProps(nextProps) {
        let {graphData, maxAge, maxFreq} = this.generateGraphData(nextProps.data);
        
        this.setState({
            graphData,
            options: {

                chartArea:{left:'20%',top:'10%',width:'100%',height:'80%'},
                hAxis: { title: 'Frequency', minValue: 0, maxValue: maxFreq },
                vAxis: { title:'Age', minValue: 0, maxValue: maxAge }
            }
        })
        console.log(maxFreq, maxAge)
    }

    generateGraphData = (personList) => {
        let graphData = [["Age", "Frequency"], [0, 0]];

        let ageFrequencies = {};
        let maxAge = 0, maxFreq = 0;

        personList.forEach(person => {
            
            let index = (isNaN(person.age)) ? 0 : person.age.toString();
            let freq = ageFrequencies[index]

            if (maxAge < index) maxAge = index;
            if (maxFreq < freq) maxFreq = freq;

            if (freq !== undefined) {
                ageFrequencies[index] += 1;
            } else {
                ageFrequencies[index] = 1;
            }

        })

        for (let key in ageFrequencies) {
            graphData.push([key, ageFrequencies[key]])
        }

        return {graphData, maxAge, maxFreq};
    }

    render() {
        return (

            <div id="graph_modal" className="modal fade" role="dialog">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal">&times;</button>
                            <h4 className="modal-title">Distribution of Age</h4>
                        </div>
                        <div className="modal-body">
                            <Chart
                                chartType="BarChart"
                                data={this.state.graphData}
                                options={this.state.options}
                                graph_id="BarChart"
                                
                                legend_toggle
                            />
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}