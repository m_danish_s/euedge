import React from "react"
import {StateManager} from "../../AppState";

export default class FormComponent extends React.Component {

    handleSubmit = (event)=>{
        event.preventDefault();

        let form = event.target;
        let person = {
            name:form.name.value,
            job: form.job_title.value,
            nick: form.nickname.value,
            age:form.age.value,
            employee:form.is_employee.checked
        }
        
        global.jQuery('#add_person_modal').modal('hide')
        StateManager.addPerson(person)
    }

    render() {
        return (

            <div id="add_person_modal" className="modal fade" role="dialog">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal">&times;</button>
                            <h4 className="modal-title">Add a Person</h4>
                        </div>
                        <form className="form-horizontal" onSubmit={this.handleSubmit}>
                            <div className="modal-body">
                                <div className="form-group">
                                    <label className="control-label col-sm-2" htmlFor="email">Name:</label>
                                    <div className="col-sm-10">
                                        <input type="text" className="form-control" name="name" placeholder="John Doe" required/>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label className="control-label col-sm-2" htmlFor="email">Job Title:</label>
                                    <div className="col-sm-10">
                                        <input type="text" className="form-control" name="job_title" placeholder="Senior Frontend Developer" required/>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label className="control-label col-sm-2" htmlFor="email">Age:</label>
                                    <div className="col-sm-10">
                                        <input type="number" min="0" className="form-control" name="age" placeholder="26" required/>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label className="control-label col-sm-2" htmlFor="email">Nickname:</label>
                                    <div className="col-sm-10">
                                        <input type="text" className="form-control" name="nickname" placeholder="Johnny" required/>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label className="control-label col-sm-2" htmlFor="email">Employee:</label>
                                    <div className="col-sm-10">
                                        <input type="checkbox" name="is_employee"/>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="submit" className="btn btn-primary">Ok</button>
                                <button type="button" className="btn btn-default" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        )
    }
}