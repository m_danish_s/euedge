import React from "react"
import PropTypes from "prop-types"

export default class DataDumpComponent extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            dump: this.getDump()
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            dump : this.getDump(nextProps.data)
        })
    }

    getDump = (data)=>{
        if(!data) data= [];
        
        let str = "["
        
        for(let i=0; i< data.length-1; i++){
            str += (JSON.stringify(data[i]) + ",\n")
        }

        if(data.length !== 0){
            str += JSON.stringify(data[data.length-1])
        }
        str += "]"
        return str;
    }

    render() {
        return (
            <textarea className="form-control" readOnly={true} rows="10" value={this.state.dump} />
        )
    }
}

DataDumpComponent.propTypes = {
    data: PropTypes.array.isRequired
}