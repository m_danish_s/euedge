import React from "react"
import PropTypes from "prop-types"
import {StateManager} from "../../AppState";

export default class TableItem extends React.Component {


    onCheckedClicked = ()=>{
        let data = this.props.data;
        data.employee = !data.employee;
        StateManager.updatePersons(this.props.index, data);
    }

    onDeleteClicked = () => {
        StateManager.deletePerson(this.props.index)
    }

    render() {
        return (
            <tr>
                <td>{this.props.data.name}<br/>{this.props.data.job}</td>
                <td>{this.props.data.age}</td>
                <td>{this.props.data.nick}</td>
                <td><input type="checkbox" checked={this.props.data.employee} onChange={this.onCheckedClicked} /></td>
                <td> <a style={{cursor:'pointer'}} onClick={this.onDeleteClicked}>Delete</a></td>
            </tr>
        )
    }
}

TableItem.propTypes = {
    data: PropTypes.object.isRequired
}