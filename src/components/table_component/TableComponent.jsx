import React from "react"
import TableItem from "./TableItem"

export default class TableComponent extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            data: props.data
        }
    }

    renderTableItems = ()=>{
        return this.props.data.map( (item, i) => {
            
            return <TableItem key={i} index={i} data={item}/>
        })
    }

    render() {
        return (
            <table className="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Name<br/>(Job Title)</th>
                        <th>Age</th>
                        <th>Nickname</th>
                        <th>Employee</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {this.renderTableItems()}
                </tbody>
            </table>
        )
    }
}